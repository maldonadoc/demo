package com.mx.kavak.microservices.example.demo.controllers

import com.mx.kavak.microservices.example.demo.dtos.HelloDTO
import spock.lang.Specification

class HelloControllerSpec extends Specification {

    HelloController controller

    def setup(){
        controller = new HelloController()
    }


    void "should return a hello dto obj"(){
        when:
      def result =  controller.helloEntryPoint()

        then:
          result instanceof HelloDTO
          result.phrase == "aca andamos"

    }
}
