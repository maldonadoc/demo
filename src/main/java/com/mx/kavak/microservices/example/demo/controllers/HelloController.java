package com.mx.kavak.microservices.example.demo.controllers;

import com.mx.kavak.microservices.example.demo.dtos.HelloDTO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @GetMapping("/hello")
    public HelloDTO helloEntryPoint(){
        HelloDTO hello = new HelloDTO();
        hello.setPhrase("aca andamos");
        return hello;
    }
}
