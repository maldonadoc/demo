package com.mx.kavak.microservices.example.demo.dtos;

public class HelloDTO {

    private String phrase;

    public String getPhrase() {
        return phrase;
    }

    public void setPhrase(String phrase) {
        this.phrase = phrase;
    }
}
